---
title: Java 8 Stream to Single Element
description: Java 8 stream Collector to return a single element as either an Optional (toOptional()) or Nullable (toNullable())
layout: post
categories: [java]
---

A situation I find myself in from time to time is needing to filter a `List` down to what I expect to be either a single element or nothing.

What I'm aiming for is to be able to do something like this ...

```java
return states.stream()
        .filter(State::isActive())
        .collect(toOptional())
        .orElseThrow();
```

Lets get started.

Just like [`toList()`](https://docs.oracle.com/javase/8/docs/api/java/util/stream/Collectors.html#toList--) I'm going to implement this as a `static` function ...

```java
public class StreamUtils {
    public static <T> Collector<T, ?, Optional<T>> toOptional() {
        // TODO
    }
}
```

Now what? Well, there are a couple of options but here I'm going to go with the simplest; use [`collectingAndThen(...)`](https://docs.oracle.com/javase/8/docs/api/java/util/stream/Collectors.html#collectingAndThen-java.util.stream.Collector-java.util.function.Function-) to collect to a `List` and then find the only element we expect ...

```java
return collectingAndThen(toList(), StreamUtils::findOnly);
```

That leaves `findOnly(...)` which should take the `List` and reduce it down to an `Optional`.

This `Optional` will contain the only element in the `Collection` if there is one.

If the `Collection` has more than one element then we `throw`.

```java
private static <T> Optional<T> findOnly(Collection<T> collection) {
    if (collection.size() > 1) {
        throw new IllegalStateException("Expected zero or one element");
    }
    return collection.stream().findFirst();
}
```

And there we have it - done!

#### Bonus Round

For cases where `null` would be preferable to an empty `Optional` ...

```java
public static <T> Collector<T, ?, T> toNullable() {
    return collectingAndThen(toList(), l -> findOnly(l).orElse(null));
}
```
